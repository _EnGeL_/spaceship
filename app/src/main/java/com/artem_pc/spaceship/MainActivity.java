package com.artem_pc.spaceship;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new SpaceView(this));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#191970")));
    }
}
