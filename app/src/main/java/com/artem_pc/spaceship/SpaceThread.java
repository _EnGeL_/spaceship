package com.artem_pc.spaceship;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

//Thread for SpaceView

public class SpaceThread extends Thread {

    private SurfaceHolder surfaceHolder;

    private Paint backgroundBlack = new Paint();
    private Paint background = new Paint();

    private boolean isWorking = true;

    private Bitmap star;
    private Bitmap spaceShip;
    private Bitmap backgroundOfActivity;

    private int spaceShipDirectionX = 100;
    private int spaceShipDirectionY = 100;

    {
        backgroundBlack.setColor(Color.BLACK);
        backgroundBlack.setStyle(Paint.Style.FILL);
    }

    {
        background.setColor(Color.argb(255, 255, 255, 255));
        background.setStyle(Paint.Style.FILL);
    }

    public SpaceThread (Context context, SurfaceHolder surfaceHolder) {
        star = BitmapFactory.decodeResource(context.getResources(), R.drawable.star);
        spaceShip = BitmapFactory.decodeResource(context.getResources(), R.drawable.spaceship);
        backgroundOfActivity = BitmapFactory.decodeResource(context.getResources(), R.drawable.back);
        this.surfaceHolder = surfaceHolder;
    }

    public void block () {
        isWorking = false;
    }

    public void setSpaceShipDirection (int x, int y) {
        spaceShipDirectionX = x;
        spaceShipDirectionY = y;
    }

    @Override
    public void run () {
        Canvas canvas;
        int shipX = 100;
        int shipY = 100;
        int[] starX = new int[12];
        int[] starY = new int[12];
        boolean isFirstRun = true;
        int speedX = 25;
        int speedY = 25;
        while (isWorking) {
            canvas = surfaceHolder.lockCanvas();
            if (isFirstRun) {
                    starX = new int[]{canvas.getWidth() / 7, canvas.getWidth() / 6, canvas.getWidth() / 5 - 20, canvas.getWidth() / 5, canvas.getWidth() / 3, canvas.getWidth() / 2 - 20, canvas.getWidth() / 2 + 20, canvas.getWidth() / 2 + 60, canvas.getWidth() / 2 + 120, canvas.getWidth() - 120, canvas.getWidth() - 60, canvas.getWidth()};
                    starY = new int[]{198, 64, 497, 264, 736, 526, 925, 1024, 836, 1336, 1175, 1036};
                    isFirstRun = false;
            }
            if (canvas != null) {
                if (canvas.getHeight() > canvas.getWidth()) {
                    try {
                        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), backgroundBlack);
                        canvas.drawBitmap(backgroundOfActivity, -200, 0, backgroundBlack);
                        canvas.drawBitmap(star, starX[1], starY[1], background);
                        if (starY[1] < canvas.getHeight()) {
                            starY[1] += 3;
                        } else {
                            starY[1] = 0;
                        }
                        canvas.drawBitmap(star, starX[2], starY[2], background);
                        if (starY[2] < canvas.getHeight()) {
                            starY[2] += 2;
                        } else {
                            starY[2] = 0;
                        }
                        canvas.drawBitmap(star, starX[3], starY[3], background);
                        if (starY[3] < canvas.getHeight()) {
                            starY[3] += 3;
                        } else {
                            starY[3] = 0;
                        }
                        canvas.drawBitmap(star, starX[4], starY[4], background);
                        if (starY[4] < canvas.getHeight()) {
                            starY[4] += 1;
                        } else {
                            starY[4] = 0;
                        }
                        canvas.drawBitmap(star, starX[5], starY[5], background);
                        if (starY[5] < canvas.getHeight()) {
                            starY[5] += 3;
                        } else {
                            starY[5] = 0;
                        }
                        canvas.drawBitmap(star, starX[0], starY[0], background);
                        if (starY[0] < canvas.getHeight()) {
                            starY[0] += 2;
                        } else {
                            starY[0] = 0;
                        }
                        canvas.drawBitmap(star, starX[6], starY[6], background);
                        if (starY[6] < canvas.getHeight()) {
                            starY[6] += 2;
                        } else {
                            starY[6] = 0;
                        }
                        canvas.drawBitmap(star, starX[7], starY[7], background);
                        if (starY[7] < canvas.getHeight()) {
                            starY[7] += 2;
                        } else {
                            starY[7] = 0;
                        }
                        canvas.drawBitmap(star, starX[8], starY[8], background);
                        if (starY[8] < canvas.getHeight()) {
                            starY[8] += 3;
                        } else {
                            starY[8] = 0;
                        }
                        canvas.drawBitmap(star, starX[9], starY[9], background);
                        if (starY[9] < canvas.getHeight()) {
                            starY[9] += 1;
                        } else {
                            starY[9] = 0;
                        }
                        canvas.drawBitmap(star, starX[10], starY[10], background);
                        if (starY[10] < canvas.getHeight()) {
                            starY[10] += 2;
                        } else {
                            starY[10] = 0;
                        }
                        canvas.drawBitmap(star, starX[11], starY[11], background);
                        if (starY[11] < canvas.getHeight()) {
                            starY[11] += 4;
                        } else {
                            starY[11] = 0;
                        }
                        canvas.drawBitmap(spaceShip, shipX, shipY, background);
                        if (shipX + spaceShip.getWidth() / 2 < spaceShipDirectionX) shipX += speedX;
                        if (shipX + spaceShip.getWidth() / 2 > spaceShipDirectionX) shipX -= speedX;
                        if (shipY + spaceShip.getHeight() / 2 < spaceShipDirectionY) shipY += speedY;
                        if (shipY + spaceShip.getHeight() / 2 > spaceShipDirectionY) shipY -= speedY;
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } else {
                    try {
                        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), backgroundBlack);
                        canvas.drawBitmap(backgroundOfActivity, 0, 0, backgroundBlack);
                        canvas.drawBitmap(star, starX[1], starY[1], background);
                        if (starY[1] < canvas.getHeight()) {
                            starY[1] += 3;
                        } else {
                            starY[1] = 0;
                        }
                        canvas.drawBitmap(star, starX[2], starY[2], background);
                        if (starY[2] < canvas.getHeight()) {
                            starY[2] += 2;
                        } else {
                            starY[2] = 0;
                        }
                        canvas.drawBitmap(star, starX[3], starY[3], background);
                        if (starY[3] < canvas.getHeight()) {
                            starY[3] += 3;
                        } else {
                            starY[3] = 0;
                        }
                        canvas.drawBitmap(star, starX[4], starY[4], background);
                        if (starY[4] < canvas.getHeight()) {
                            starY[4] += 1;
                        } else {
                            starY[4] = 0;
                        }
                        canvas.drawBitmap(star, starX[5], starY[5], background);
                        if (starY[5] < canvas.getHeight()) {
                            starY[5] += 3;
                        } else {
                            starY[5] = 0;
                        }
                        canvas.drawBitmap(star, starX[0], starY[0], background);
                        if (starY[0] < canvas.getHeight()) {
                            starY[0] += 2;
                        } else {
                            starY[0] = 0;
                        }
                        canvas.drawBitmap(star, starX[6], starY[6], background);
                        if (starY[6] < canvas.getHeight()) {
                            starY[6] += 2;
                        } else {
                            starY[6] = 0;
                        }
                        canvas.drawBitmap(star, starX[7], starY[7], background);
                        if (starY[7] < canvas.getHeight()) {
                            starY[7] += 1;
                        } else {
                            starY[7] = 0;
                        }
                        canvas.drawBitmap(star, starX[8], starY[8], background);
                        if (starY[8] < canvas.getHeight()) {
                            starY[8] += 1;
                        } else {
                            starY[8] = 0;
                        }
                        canvas.drawBitmap(star, starX[9], starY[9], background);
                        if (starY[9] < canvas.getHeight()) {
                            starY[9] += 2;
                        } else {
                            starY[9] = 0;
                        }
                        canvas.drawBitmap(star, starX[10], starY[10], background);
                        if (starY[10] < canvas.getHeight()) {
                            starY[10] += 1;
                        } else {
                            starY[10] = 0;
                        }
                        canvas.drawBitmap(star, starX[11], starY[11], background);
                        if (starY[11] < canvas.getHeight()) {
                            starY[11] += 4;
                        } else {
                            starY[11] = 0;
                        }
                        canvas.drawBitmap(spaceShip, shipX, shipY, background);
                        if (shipX + spaceShip.getWidth() / 2 < spaceShipDirectionX) shipX += speedX;
                        if (shipX + spaceShip.getWidth() / 2 > spaceShipDirectionX) shipX -= speedX;
                        if (shipY + spaceShip.getHeight() / 2 < spaceShipDirectionY) shipY += speedY;
                        if (shipY + spaceShip.getHeight() / 2 > spaceShipDirectionY) shipY -= speedY;
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }

        }
    }

}
