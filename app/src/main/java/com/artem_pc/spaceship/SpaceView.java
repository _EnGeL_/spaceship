package com.artem_pc.spaceship;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.SurfaceHolder;

//SurfaceView for MainActivity

public class SpaceView extends SurfaceView implements SurfaceHolder.Callback {

    SpaceThread spaceThread;

    public SpaceView (Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated (SurfaceHolder surfaceHolder) {
        spaceThread = new SpaceThread(getContext(), getHolder());
        spaceThread.start();
    }

    @Override
    public void surfaceChanged (SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed (SurfaceHolder surfaceHolder) {
        spaceThread.block();
    }

    @Override
    public boolean onTouchEvent (MotionEvent event) {
        spaceThread.setSpaceShipDirection((int)event.getX(), (int)event.getY());
        return false;
    }
}
